import OntologicalEntitiesContants.*;
import it.unife.endif.ml.bundle.utilities.QueryResult;
import it.unife.endif.ml.probowlapi.exception.ObjectNotInitializedException;
import org.semanticweb.owlapi.model.*;

import javax.print.attribute.standard.MediaSize;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws OWLException, ObjectNotInitializedException {
        //useProbActivityOnt();
        testProbability();
        //usePetOntology();
        //useCrimeAndPunishment();
    }

    private static void testProbability() throws OWLException, ObjectNotInitializedException {
        //Inizializzazione ontologia
        String ontologyName = "prob-ont-tableau-oriented-2-DIFFERENT-ROLES";
        ProbOntologyHandler ontology = new ProbOntologyHandler(ontologyName);

        //Lista degli assiomi da inserire nell'Abox
        Set<OWLAxiom> axioms = new HashSet<>();

        //--------------------------- PREPARAZIONE DATI DI CONTESTO --------------------------------------------
        //Luogo semantico
        OWLNamedIndividual currentlocationType = ontology.getNamedIndividual(Individuals.currentSymbolicLocation);
        OWLClass locationTypeClass = ontology.getClassFromOntology(SymbolicLocation.indoorLocation);
        axioms.add(ontology.getAssertionAxiom(locationTypeClass, currentlocationType));

        //Velocità
        OWLNamedIndividual currentSpeed = ontology.getNamedIndividual(Individuals.currentSpeed);
        OWLClass currentSpeedClass = ontology.getClassFromOntology(OtherEntities.mediumSpeed);
        axioms.add(ontology.getAssertionAxiom(currentSpeedClass, currentSpeed));

        //--------------------------- SETTING DELLA QUERY PROBABILISTICA --------------------------------------------
        //Attività candidata
        OWLNamedIndividual currentActivity = ontology.getNamedIndividual(Individuals.currentActivity);

        //Lego l'attività con i dati di contesto che rappresentano l'incertezza
        OWLObjectProperty isPossibleWithProp = ontology.getObjectProperty(Relationships.isGoingStairsPossibleWith);
        //axioms.add(ontology.getObjectPropertyAssertionAxiom(isPossibleWithProp, currentActivity, currentlocationType));
        //axioms.add(ontology.getObjectPropertyAssertionAxiom(isPossibleWithProp, currentActivity, currentSpeed));

        //Aggiungo gli assiomi all'ontologia
        ontology.addAxiomsToOntology(axioms);

        //Calcolo della probabilità
        OWLClass uncertainGoingStairs = ontology.getClassFromOntology("UncertainGoingStairs");
        OWLAxiom axIsPossible = ontology.getAssertionAxiom(uncertainGoingStairs, currentActivity);
        QueryResult result = ontology.computeBundleQuery(axIsPossible, true);

        System.out.println("\n" + result + "\n");

        //Faccio il dispose di Bundle e dell'Abox dell'ontologia
        ontology.disposeBundle();

        //Salvo una copia dell'ontologia e poi pulisco l'abox
        ontology.saveCopy();
        ontology.clearAbox();
    }


    /**
     * Esperimento con l'ontologia minimal-ont.owl
     * @throws OWLException
     * @throws ObjectNotInitializedException
     */
    private static void useProbActivityOnt() throws OWLException, ObjectNotInitializedException {
        //Inizializzazione ontologia
        String ontologyName = "prob-ont-tableau-oriented-1";
        ProbOntologyHandler ontology = new ProbOntologyHandler(ontologyName);

        //Lista degli assiomi da inserire nell'Abox
        Set<OWLAxiom> axioms = new HashSet<>();

        //------------------ INIZIALIZZAZIONE INDIVIDUI E CONTESTO DELL'UTENTE ------------------------------------
        //Utente corrente
        OWLNamedIndividual currentPerson = ontology.getNamedIndividual(Individuals.currentPerson);
        OWLClass personClass = ontology.getClassFromOntology(OtherEntities.person);
        OWLClassAssertionAxiom axPerson = ontology.getAssertionAxiom(personClass, currentPerson);
        axioms.add(axPerson);

        //Luogo google corrente (impostato forzatamente a "street_address")
        OWLNamedIndividual currentGooglePlace = ontology.getNamedIndividual(Individuals.currentGooglePlace);
        OWLClass placeClass = ontology.getClassFromOntology("street_address");

        OWLNamedIndividual currentSymbolicLocation = ontology.getNamedIndividual(Individuals.currentSymbolicLocation);
        OWLClass semanticPlaceClass = ontology.getClassFromOntology(SymbolicLocation.outdoorLocation);

        OWLClassAssertionAxiom axGooglePlace = ontology.getAssertionAxiom(placeClass, currentGooglePlace);
        OWLClassAssertionAxiom axSemanticPlace = ontology.getAssertionAxiom(semanticPlaceClass, currentSymbolicLocation);

        //Lego il luogo google alla specifica location simbolica (in questo caso outdoor location)
        OWLObjectProperty usesProperty = ontology.getObjectProperty(Relationships.hasLocationType);
        OWLObjectPropertyAssertionAxiom axUses =
                ontology.getObjectPropertyAssertionAxiom(usesProperty, currentGooglePlace, currentSymbolicLocation);

        axioms.add(axGooglePlace); axioms.add(axSemanticPlace); axioms.add(axUses);

        //Variazione di velocità (imposto "lowSpeed")
        OWLNamedIndividual currentSpeed = ontology.getNamedIndividual(Individuals.currentSpeed);
        OWLClass speedClass = ontology.getClassFromOntology(OtherEntities.positiveSpeed);
        OWLClassAssertionAxiom axSpeed = ontology.getAssertionAxiom(speedClass, currentSpeed);

        OWLObjectProperty hasSpeed = ontology.getObjectProperty(Relationships.hasCurrentSpeed);
        OWLObjectPropertyAssertionAxiom axHasSpeed =
                ontology.getObjectPropertyAssertionAxiom(hasSpeed, currentPerson, currentSpeed);

        axioms.add(axSpeed); axioms.add(axHasSpeed);

        //Variazione di altezza (imposto "PositiveMediumHeight")
        OWLNamedIndividual currentHeight = ontology.getNamedIndividual(Individuals.currentHeightVariation);
        OWLClass heightClass = ontology.getClassFromOntology(OtherEntities.positiveHeightVariation);
        OWLClassAssertionAxiom axHeight = ontology.getAssertionAxiom(heightClass, currentHeight);

        OWLObjectProperty hasHeightVariation = ontology.getObjectProperty(Relationships.hasCurrentHeightVariation);
        OWLObjectPropertyAssertionAxiom axHasHeight =
                ontology.getObjectPropertyAssertionAxiom(hasHeightVariation, currentPerson, currentHeight);

        axioms.add(axHeight); axioms.add(axHasHeight);

        //------- COSTRUZIONE DELLA QUERY: CON CHE PROBABILITA' L'UTENTE STA FACENDO GOING STAIRS? -------------

        //Genero un'individuo generico rappresentante l'attività corrente e la associo ad una classe generica che
        //creo dinamicamente nell'ontologia
        OWLNamedIndividual currentActivity = ontology.getNamedIndividual(Individuals.currentActivity);

        //Lego l'attività all'utente che la svolge
        OWLObjectProperty hasActorProp = ontology.getObjectProperty(Relationships.hasActor);
        OWLObjectPropertyAssertionAxiom axHasActor =
                ontology.getObjectPropertyAssertionAxiom(hasActorProp, currentActivity, currentPerson);

        //Lego l'attività al luogo in cui viene svolta
        OWLObjectProperty canTakePlaceOn = ontology.getObjectProperty(Relationships.canTakePlaceOn);
        OWLObjectPropertyAssertionAxiom axCanTakePlaceOn =
                ontology.getObjectPropertyAssertionAxiom(canTakePlaceOn, currentActivity, currentGooglePlace);

        axioms.add(axCanTakePlaceOn);
        axioms.add(axHasActor);

        //Avvaloro l'ontologia con tutti gli assiomi dichiarati
        ontology.addAxiomsToOntology(axioms);

        ontology.saveCopy();

        //Calcolo della probabilità
        OWLClass goingStairs = ontology.getClassFromOntology(Activities.walking);
        OWLAxiom axIsPossible = ontology.getAssertionAxiom(goingStairs, currentActivity);
        QueryResult result = ontology.computeBundleQuery(axIsPossible, true);

        System.out.println("\n" + result + "\n");

        //Faccio il dispose di Bundle e dell'Abox dell'ontologia
        ontology.disposeBundle();
      //  ontology.clearAbox();
    }

    /**
     * Esperimento con crime_and_punishment.owl ontology
     * @throws OWLException
     * @throws ObjectNotInitializedException
     */
    private static void useCrimeAndPunishment() throws OWLException, ObjectNotInitializedException {
        //Inizializzazione ontologia
        String ontologyName = "crime_and_punishment";
        ProbOntologyHandler ontology = new ProbOntologyHandler(ontologyName);

        //Inizializzo l'individuo che mi serve per fare la query
        OWLNamedIndividual raskolnikov = ontology.getNamedIndividual("raskolnikov");

        //inizializzo la classe di cui voglio valutare l'appartenenza
        OWLClass greatMan = ontology.getClassFromOntology("GreatMan");

        //Creo la query: raskolnikov è un GreatMan?
        OWLAxiom query = ontology.getAssertionAxiom(greatMan, raskolnikov);

        //Calcolo il risultato della query con la sua probabilità, stampandola
        QueryResult result = ontology.computeBundleQuery(query, true);
        System.out.println("\n" + result + "\n");

        ontology.disposeBundle();
    }

    /**
     * Esperimento con pet_ontology.owl
     * @throws OWLException
     * @throws ObjectNotInitializedException
     */
    private static void usePetOntology() throws OWLException, ObjectNotInitializedException {
        String ontologyName = "pet_ontology";
        ProbOntologyHandler ontology = new ProbOntologyHandler(ontologyName);

        //Inizializzo l'individuo che mi serve per fare la query
        OWLNamedIndividual kevin = ontology.getNamedIndividual("kevin");

        //inizializzo la classe di cui voglio valutare l'appartenenza
        OWLClass greatMan = ontology.getClassFromOntology("NatureLover");

        //Creo la query: raskolnikov è un GreatMan?
        OWLAxiom query = ontology.getAssertionAxiom(greatMan, kevin);

        //Calcolo il risultato della query con la sua probabilità, stampandola
        QueryResult result = ontology.computeBundleQuery(query, true);
        System.out.println("\n" + result + "\n");

        ontology.disposeBundle();
    }
}
