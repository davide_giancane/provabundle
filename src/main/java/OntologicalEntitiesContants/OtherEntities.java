package OntologicalEntitiesContants;

/**
 * Costanti per altre tipologie di entità presenti nell'ontologia,
 * che vanno da oggetti Artifact a Vehicle, HeightVariation, ecc.
 */
public class OtherEntities {
    public static final String vehicle = "Vehicle";
    public static final String privateVehicle = "PrivateVehicle";
    public static final String publicVehicle = "PublicVehicle";
    public static final String bike = "Bike";
    public static final String car = "Car";

    public static final String communicationRoute = "CommunicationRoute";
    public static final String indoorCommunicationRoute = "IndoorCommunicationRoute";
    public static final String arcade = "Arcade";
    public static final String corridor = "Corridor";
    public static final String elevator = "Elevator";
    public static final String stairs = "Stairs";
    public static final String outdoorCommunicationRoute = "OutdoorCommunicationRoute";
    public static final String promenade = "Promenade";
    public static final String rails = "Rails";
    public static final String road = "Road";
    public static final String sidewalk = "Sidewalk";
    public static final String steps = "Steps";
    public static final String street = "Street";
    public static final String track = "Track";

    public static final String environment = "Environment";
    public static final String place = "Place";
    public static final String darkPlace = "DarkPlace";
    public static final String lightPlace = "LightPlace";
    public static final String temperature = "Temperature";

    public static final String heightVariation = "HeightVariation";
    public static final String negativeHeightVariation = "NegativeHeightVariation";
    public static final String nullHeightVariation = "NullHeightVariation";
    public static final String positiveHeightVariation = "PositiveHeightVariation";
    public static final String positiveMediumHeight = "PositiveMediumHeight";
    public static final String positiveHighHeight = "PositiveHighHeight";
    public static final String negativeMediumHeight = "NegativeMediumHeight";


    public static final String movingObject = "MovingObject";
    public static final String person = "Person";

    public static final String posture = "Posture";
    public static final String lyingPosture = "LyingPosture";
    public static final String seatedPosture = "SeatedPosture";
    public static final String standingPosture = "StandingPosture";

    public static final String route = "Route";
    public static final String nonPublicTransportRoute = "NonPublicTransportRoute";
    public static final String publicTransportRoute = "PublicTransportRoute";

    public static final String speed = "Speed";
    public static final String nullSpeed = "NullSpeed";
    public static final String positiveSpeed = "PositiveSpeed";
    public static final String lowSpeed = "LowSpeed";
    public static final String highSpeed = "HighSpeed";

    public static final String timeGranularity = "TimeGranularity";
    public static final String day = "Day";
    public static final String holiday = "Holiday";
    public static final String weekday = "Weekday";
    public static final String timeOfDay = "TimeOfDay";
    public static final String afternoon = "Afternoon";
    public static final String dawn = "Dawn";
    public static final String dusk = "Dusk";
    public static final String evening = "Evening";
    public static final String morning = "Morning";
    public static final String night = "Night";
    public static final String noon = "Noon";
    public static final String lunch = "LunchTime";
    public static final String mediumSpeed = "MediumSpeed";
}
