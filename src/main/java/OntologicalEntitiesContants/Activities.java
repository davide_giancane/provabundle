package OntologicalEntitiesContants;

/**
 * Costanti per i nomi delle attività presenti nell'ontologia
 */
public class Activities {
    public static final String individualActivity = "IndividualActivity";

    public static final String brushingTeeth = "BrushingTeeth";

    public static final String goingStairs = "GoingStairs";
    public static final String goingUpstairs = "GoingUpstairs";
    public static final String goingDownstairs = "GoingDownstairs";
    public static final String lying = "Lying";
    public static final String onElevator = "onElevator";
    public static final String elevatorDown = "ElevatorDown";
    public static final String elevatorUp = "ElevatorUp";
    public static final String running = "Running";
    public static final String sitting = "Sitting";
    public static final String standing = "Standing";
    public static final String strolling = "Strolling";
    public static final String walking = "Walking";

    public static final String cycling = "Cycling";
    public static final String movingByCar = "MovingByCar";
    public static final String sittingTransport = "SittingTransport";
    public static final String standingTransport = "StandingTransport";
    public static final String eating = "Eating";
    public static final String activity = "Activity";
}