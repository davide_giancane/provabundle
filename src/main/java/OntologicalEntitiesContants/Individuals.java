package OntologicalEntitiesContants;

/**
 * Costanti per rappresentare gli individui che popolano l'ABOX
 * dell'ontologia in fase di istanziazione dell'attività da
 * analizzare e del contesto
 */
public class Individuals {
    public static final String currentActivity = "currentActivity";
    public static final String currentSymbolicLocation = "currentSymbolicLocation";
    public static final String currentSpeed = "currentSpeed";
    public static final String currentHeightVariation = "currentHeightVariation";

    public static final String currentPerson = "currentPerson";
    public static final String currentGooglePlace = "currentGooglePlace";
    public static final String currentDayType = "currentDayType";
    public static final String currentMonth = "currentMonth";
    public static final String currentSeason = "currentSeason";
    public static String currentBuilding = "currentBuilding";
    public static String currentLocation = "currentLocation";
    public static String currentBike = "currentBike";
    public static String currentRoute = "currentRoute";
    public static String currentCar = "currentCar";
    public static String currentVehicle = "currentVehicle";
    public static String currentPosture = "currentPosture";
    public static String currentElevator = "currentElevator";
    public static String currentStairs = "currentStairs";
}
