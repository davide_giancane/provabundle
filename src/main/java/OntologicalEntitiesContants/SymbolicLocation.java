package OntologicalEntitiesContants;

/**
 * Costanti per le tipologie di luoghi simbolici
 * modellati dall'ontologia
 */
public class SymbolicLocation {
    public static final String symbolicLocation = "SymbolicLocation";

    //Indoor Locations
    public static final String indoorLocation = "IndoorLocation";
    public static final String building = "Building";
    public static final String bureaux = "BureauxBuilding";
    public static final String campus = "CampusBuilding";
    public static final String cinema = "Cinema";
    public static final String civilCorpStation = "CivilCorpStation";

    public static final String commercial = "CommercialBuilding";
    public static final String arcade = "ArcadeBuilding";
    public static final String indoorSportsCenter = "IndoorSportsCenter";
    public static final String mallBuilding = "MallBuilding";
    public static final String store = "StoreBuilding";

    public static final String historical = "HistoricalBuilding";
    public static final String home = "HomeBuilding";
    public static final String hospital = "HospitalBuilding";
    public static final String religious = "ReligiousBuilding";
    public static final String terminal = "Terminal";
    public static final String warehouse = "Warehouse";

    //Rooms
    public static final String room = "Room";
    public static final String bedRoom = "BedRoom";
    public static final String breakRoom = "BreakRoom";
    public static final String classRoom = "ClassRoom";
    public static final String conferenceRoom = "ConferenceRoom";
    public static final String diningRoom = "DiningRoom";
    public static final String hospitalRoom = "HospitalRoom";
    public static final String kitchen = "Kitchen";
    public static final String laboratory = "Laboratory";
    public static final String livingRoom = "LivingRoom";
    public static final String meetingRoom = "MeetingRoom";
    public static final String office = "Office";
    public static final String restRoom = "RestRoom";

    //OutdoorLocations
    public static final String outdoorLocation = "OutdoorLocation";
    public static final String nonPedestrian = "NonPedestrianOutdoorLocation";
    public static final String pedestrian = "PedestrianOutdoorLocation";
}
