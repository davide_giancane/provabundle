package OntologicalEntitiesContants;

/**
 * Costanti per rappresentare le object properties modellate
 * nell'ontologia
 */
public class Relationships {
    public static final String canBeUsedAs = "canBeUsedAs";
    public static final String canHostActivity = "canHostActivity";
    public static final String canTakePlaceDuring = "canTakePlaceDuring";
    public static final String canTakePlaceOn = "canTakePlaceOn";

    public static final String contains = "contains";
    public static final String hasArtifact = "hasArtifact";
    public static final String hasRoomInside = "hasRoomInside";
    public static final String includesCommunicationRoute = "includesCommunicationRoute";

    public static final String hasActor = "hasActor";
    public static final String hasCurrentActivity = "hasCurrentActivity";
    public static final String hasCurrentHeightVariation = "hasCurrentHeightVariation";
    public static final String hasCurrentPosture = "hasCurrentPosture";
    public static final String hasCurrentSpeed = "hasCurrentSpeed";
    public static final String hasCurrentSymbolicLocation = "hasCurrentSymbolicLocation";

    public static final String hasLightFixture = "hasLightFixture";
    public static final String hasRoute = "hasRoute";
    public static final String hasTimeGranularity = "hasTimeGranularity";
    public static final String homeBuildingOf = "HomeBuildingOf";
    public static final String hosts = "hosts";
    public static final String isCoveredBy = "isCoveredBy";

    public static final String isInside = "isInside";
    public static final String isArtifactIn = "isArtifactIn";
    public static final String isCommunicationRouteOf = "isCommunicationRouteOf";
    public static final String isInsideBuilding = "isInsideBuilding";

    public static final String isMovingOn = "isMovingOn";
    public static final String livesInBuilding = "livesInBuilding";
    public static final String lookingAt = "lookingAt";
    public static final String measuredTemperature = "measuredTemperature";

    public static final String hasLocationType = "hasLocationType";
    public static final String usingArtifact = "usingArtifact";

    public static final String worksIn = "worksIn";

    //RELAZIONI SU INCERTEZZA
    public static final String isPossibleWith = "isPossibleWith";
    public static final String isGoingStairsPossibleWith = "isGoingStairsPossibleWith";
}
