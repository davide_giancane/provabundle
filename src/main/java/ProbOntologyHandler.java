import it.unife.endif.ml.bundle.BDDFactoryType;
import it.unife.endif.ml.bundle.Bundle;
import it.unife.endif.ml.bundle.utilities.QueryResult;
import it.unife.endif.ml.probowlapi.exception.ObjectNotInitializedException;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Set;

/**
 * Classe che gestisce l'ontologia probabilistica
 * e tutte le interazioni cone essa
 */
public class ProbOntologyHandler {
    private Bundle bundle;
    private OWLOntology ontology;
    private OWLOntologyManager manager;
    private OWLDataFactory df;
    private IRI ontologyIRI;

    private String ontName;

    /**
     * Inizializza la classe settando l'ontologia da usare,
     * il suo manager e il reasoner BUNDLE
     * @param ontName nome del file senza estensione
     */
    public ProbOntologyHandler(String ontName) {
        this.ontName = ontName;
        //Costruzione path ontologia
        String ontPath = System.getProperty("user.dir") + "/src/main/resources/" + ontName + ".owl";

        //Caricamento ontologia e sue componenti
        InputStream is = null;
        try {
            is = new FileInputStream(ontPath);
            manager = OWLManager.createOWLOntologyManager();
            ontology = manager.loadOntologyFromOntologyDocument(is);
            df = manager.getOWLDataFactory();

            ontologyIRI = IRI.create(ontology.getOntologyID().getOntologyIRI().get().toURI());
            
            //Costruzione reasoner
            bundle = new Bundle();
            bundle.setRootOntology(ontology);
            bundle.setReasonerFactory(new ReasonerFactory()); //Uso hermit
            bundle.setMaxExplanations(10); //lascio che il reasoner computi il numero massimo di spiegazioni
            bundle.init();
        } catch (FileNotFoundException e) {
            System.err.println("Path ontologia non trovato!");
        } catch (OWLOntologyCreationException e) {
            System.err.println("Ontologia non caricata");
        }
    }

    /**
     * Prende una classe specifica dall'ontologia
     * @param className nome della classe
     * @return
     */
    public OWLClass getClassFromOntology(String className){
        IRI classIRI = getEntityIRI(className);
        return df.getOWLClass(classIRI);
    }

    /**
     * Ritorna un individuo con nome
     * @param name nome dell'individuo
     * @return individuo nuovo se non esiste già nell'ontologia,
     * altrimenti quello esistente
     */
    public OWLNamedIndividual getNamedIndividual(String name){
        IRI indIRI = getEntityIRI(name);
        return df.getOWLNamedIndividual(indIRI);
    }

    public OWLOntology getOntology(){
        return this.ontology;
    }

    /**
     * Wrapper per <i>getOWLClassAssertionAxiom</i> di OWL-API.
     * Crea un'assioma asserzionale
     * @param clazz classe interessata dall'asserzione
     * @param ind individuo che si vuole asserire come appartenente a clazz
     * @return
     */
    public OWLClassAssertionAxiom getAssertionAxiom(OWLClass clazz, OWLIndividual ind){
        return df.getOWLClassAssertionAxiom(clazz, ind);
    }

    public OWLSubClassOfAxiom getSubClassOfAxiom(OWLClass subClass, OWLClass superClass){
        return df.getOWLSubClassOfAxiom(subClass, subClass);
    }

    /**
     * Wrapper per <i>getOWLObjectPropertyAssertionAxiom</i> di OWL-API.
     * Permette la creazione di una relazione fra due individui
     * @param prop proprietà che deve legare gli individui
     * @param domainIndividual individuo "domain" che è in relazione con l'altro
     * @param rangeIndividual individuo "range" che subisce la relazione con l'altro
     * @return asserzione da mettere nell'Abox
     */
    public OWLObjectPropertyAssertionAxiom getObjectPropertyAssertionAxiom(OWLObjectProperty prop,
                                                                           OWLNamedIndividual domainIndividual,
                                                                           OWLNamedIndividual rangeIndividual){
        return df.getOWLObjectPropertyAssertionAxiom(prop, domainIndividual, rangeIndividual);
    }



    /**
     * Wrapper per <i>getObjectPropertyAxiom</i> di OWL-API.
     * @param name nome della ObjectProperty desiderata
     * @return la OWLObjectProperty
     */
    public OWLObjectProperty getObjectProperty(String name){
        IRI propIRI = getEntityIRI(name);
        return df.getOWLObjectProperty(propIRI);
    }

    /**
     * Wrapper per <i>computeQuery</i> di Bundle
     * @param query interrogazione da sottoporre a Bundle e Ontologia probabilistica
     * @param printExplanation true se si vogliono stampare le spiegazioni per la query, false altrimenti
     * @return il risultato della query indicante il suo valore di probabilità
     * @throws OWLException
     * @throws ObjectNotInitializedException
     */
    public QueryResult computeBundleQuery(OWLAxiom query, boolean printExplanation) throws OWLException, ObjectNotInitializedException {
        QueryResult result = (QueryResult) bundle.computeQuery(query);
        if(printExplanation){
            int count = 1;
            for(Set<OWLAxiom> expl : result.getQueryExplanations()){
                System.out.println("\nEXPLANATION " + count++ + "\n");
                expl.forEach(owlAxiom -> System.out.println(owlAxiom));
            }
        }
        return result;
    }

    /**
     * Aggiunge un insieme di assiomi all'ontologia
     * @param axioms assiomi
     */
    public void addAxiomsToOntology(Set<OWLAxiom> axioms){
        this.manager.addAxioms(this.ontology, axioms);
        this.bundle.getReasoner().flush();
    }

    public OWLDataFactory getDf(){
        return this.df;
    }

    public void disposeBundle(){
        bundle.dispose();
    }

    /**
     * Ritorna un'entità generica dall'ontologia.
     * Può essere una classe, una assioma, un individuo o una relazione
     * @param entityName nome dell'entità da prelevare
     * @return IRI dell'entità richiesta
     */
    private IRI getEntityIRI(String entityName){
        return IRI.create(ontologyIRI + "#" + entityName);
    }

    /**
     * Pulisce l'Abox
     */
    public void clearAbox() {
        Set<OWLAxiom> aBoxAxioms = this.ontology.getABoxAxioms(Imports.EXCLUDED);
        this.manager.removeAxioms(this.ontology, aBoxAxioms);
    }

    /**
     * Salva una copia dell'ontologia in uso
     */
    public void saveCopy(){
        File file = new File(System.getProperty("user.dir") + "/src/main/resources/" + ontName + "-copy" + ".owl");
        OWLDocumentFormat format = manager.getOntologyFormat(ontology);
        try {
            manager.saveOntology(ontology, format, IRI.create(file.toURI()));
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
        }
    }
}
